<?php
/*Užduotis 8
•	Kazys ir Petras žaidžiai šaškėm. Petras surenka taškų kiekį nuo 10 iki 20, Kazys surenka taškų kiekį nuo 5 iki 25. 
•	Vienoje eilutėje išvesti žaidėjų vardus su taškų kiekiu ir “Partiją laimėjo: laimėtojo vardas”; 
•	Taškų kiekį generuokite funkcija rand();
•	Žaidimą laimi tas, kas greičiau surenka 222 taškus. Partijas kartoti tol, kol kažkuris žaidėjas pirmas surenka 222 arba daugiau taškų.
*/

$player1 = 0; //Petras
$player2 = 0;  //Kazys

do {
    $player1 = $player1 + rand(10, 20);
    $player2 = $player2 + rand(5, 25);
    echo 'Petras: ' . $player1 . ' ; ' . 'Kazys: ' . $player2 . PHP_EOL;
} while ($player1 < 222 && $player2 < 222);

if ($player1 < $player2) {
    echo "Partiją laimėjo: Kazys.";
} elseif ($player1 === $player2) {
    echo "Lygiosios.";
} else {
    echo "Partiją laimėjo: Petras.";
}
