<?php
/*Užduotis 9
Skaičiams nuo 1 iki 100 išvesti:
•	Jeigu skaičius dalijasi iš 5 išvesti "Fizz";
•	Jeigu skaičius dalijasi iš 7 išvesti "Buzz"
•	Jeigu skaičius nesidalina nei iš 5 nei iš 7 išvesti patį skaičių
•	Jeigu skaičius dalijasi ir iš 5 ir iš 7 išvesti "Fizz Buzz"
Rezultatas turėtų būti: 1, 2, 3, 4, Fizz, 6, Buzz, 8, 9, Fizz, 11, 12, 13, Buzz, Fizz, 16, 17, 18, 19, Fizz, Buzz,22, 23, 24, Fizz, 26, 27, Buzz, 29, Fizz, 31, 32, 33, 34, Fizz Buzz, 36, 37, ...
*/

for ($i = 1; $i <= 100; $i++) {
    $modalFive = $i % 5;
    $modalSeven = $i % 7;
    if ($modalFive === 0 && $modalSeven === 0) {
        echo "Fizz Buzz, ";
    } elseif ($modalFive === 0) {
        echo "Fizz, ";
    } elseif ($modalSeven === 0) {
        echo "Buzz, ";
    } else {
        echo $i . ", ";
    }
}


