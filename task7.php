<?php
/*Užduotis 7
Metam monetą. Monetos kritimo rezultatą imituojam rand() funkcija, kur 0 yra herbas, o 1 - skaičius.
Monetos metimo rezultatus išvedame į ekraną atskiroje eilutėje: “S” jeigu iškrito skaičius ir “H” jeigu herbas.
Suprogramuokite keturis skirtingus scenarijus kai monetos metimą stabdome:
•    Tris kartus iš eilės iškrintus herbui;
•    Iškrentus herbui;
•    Iškrentus skaičiui;
•    Tris kartus iškrentus skaičiui;
 */

//0 - H; 1 - S

/*
1. Stabdom iskritus herbui
while ($coin = rand(0, 1) != 0) {
echo "$coin S" . PHP_EOL;
}
 */

/*
2. Stabdom iskritus skaiciui
while ($coin = rand(0, 1) != 1) {
echo "$coin H" . PHP_EOL;
}
 */

//3. Stabdom iskritus 3 herbams, t.y. 0

$x = 0;
$theEnd = false;
$arr[0] = rand(0, 1);
$arr[1] = rand(0, 1);
$arr[2] = rand(0, 1);
$string = showCoin($arr[0]) . '  ' . showCoin($arr[1]) . '  ' . showCoin($arr[2]) . '  ' . PHP_EOL;
echo $string;

do {
    if (($arr[$x] == $arr[$x + 1]) && ($arr[$x] == $arr[$x + 2]) && ($arr[$x] == 0)) {
        echo "Three herbs in a row!";
        $theEnd = true;
        break;
    }
    $x++;
    $arr[$x + 2] = rand(0, 1);
    echo "New coin: " . showCoin($arr[$x + 2]) . PHP_EOL;

} while ($theEnd == false);

/*4. Stabdom iskritus 3 skaiciams (ne is eiles)
$y = 1;
do {
    $coin = rand(0, 1);
    if ($coin == 1) {
        echo showCoin($coin) . PHP_EOL;
        $y++;
    } else {
        echo showCoin($coin) . PHP_EOL;
    }
} while ($y <= 3);
*/

function showCoin($coinNumber)
{
    if ($coinNumber == 1) {
        return "S";
        $y++;
    } else {
        return "H";
    }
}
