<?php

/*Užduotis 5
Parašykite skirptą, kuris skaito iš failo patalpinto php serveryje po vieną eilutę.
	Išveskite eilutės numerį ir eilutę
	Praleiskite tuščias eilutes
	Praleiskite visas kitas eilutes (nustokite skaitymą), jeigu eilutės tekstas yra "exit";
*/

$handle = fopen("inputfile.txt", "r");
if ($handle) {
    while (($line = fgets($handle)) !== false) {
        if (trim($line) != '') {
            if (trim($line) != "exit") {
                echo $line;
            } else {
                break;
            }           
        } else {
            continue;
        }        
    }
    fclose($handle);
} else {
    echo "Unable to open the file";
} 

?>